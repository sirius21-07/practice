package src.ru.sirius.daniil;

public class Main {
    public static void main(String[] args) {
        Military[] militaries = new Military[10];
        militaries[0] = new Soldier();
        militaries[1] = new Soldier();
        militaries[2] = new Ensign();
        militaries[3] = new Ensign();
        militaries[4] = new Ensign();
        militaries[5] = new Oficer();
        militaries[6] = new Oficer();
        militaries[7] = new OficerPilot();
        militaries[8] = new OficerPodvodnic();
        militaries[9] = new OficerPodvodnic();

        int sumDamage = 0;
        int i = 0;
        do {
            sumDamage = sumDamage + militaries[i].damage;
            i++;
        } while (i < 10);
        System.out.println(sumDamage);
        int n = 0;
        i = 0;
        do {
            if (militaries[i] instanceof OficerPilot) {
                n++;
            }
            if (militaries[i] instanceof OficerPodvodnic) {
                n++;
            }
            i++;
        } while (i < 10);
        System.out.println(n);
    }
}
