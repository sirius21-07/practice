package animals;

import java.util.Random;

public class Main {

    private static Random random = new Random();

    public static void main(String[] args) {
        initAnimals();
    }

    public static void initAnimals() {
        int animalsTotalSize = 0;
        Snake[] snakes = new Snake[random.nextInt(5) + 5];
        Rabbit[] rabbits = new Rabbit[random.nextInt(5) + 5];
        Mouse[] mouses = new Mouse[random.nextInt(5) + 5];

        for (Snake snake : snakes) {
            snake = new Snake();
            animalsTotalSize += snake.getSize();
        }
        for (Rabbit rabbit : rabbits) {
            rabbit = new Rabbit();
            animalsTotalSize += rabbit.getSize();
        }
        for (Mouse mouse : mouses) {
            mouse = new Mouse();
            animalsTotalSize += mouse.getSize();
        }

        System.out.println("Кол-во коробок:" + animalsTotalSize / 6);
    }
}
