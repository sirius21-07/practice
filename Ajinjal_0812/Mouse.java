
package animals;

public class Mouse extends Animal {

    public Mouse() {
        this.size = 1;
    }

    @Override
    public String toString() {
        return "Mouse{" +
                "size=" + size +
                '}';
    }
}
