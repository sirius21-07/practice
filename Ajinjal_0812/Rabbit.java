package animals;

public class Rabbit extends Animal {

    public Rabbit() {
        this.size = 3;
    }

    @Override
    public String toString() {
        return "Rabbit{" +
                "size=" + size +
                '}';
    }
}
