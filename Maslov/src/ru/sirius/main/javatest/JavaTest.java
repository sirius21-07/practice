package ru.sirius.main.javatest;
import java.util.Random;


public class JavaTest {
    public static void main(String[] args){
        Students StudentA = new Students ("Bogdan", ((int) (Math.random() * 100)), 2);
        Students StudentB = new Students ("Roma", ((int) (Math.random() * 100)), 2);
        Students StudentC = new Students ("Oleg", ((int) (Math.random() * 100)), 1);
        Students StudentD = new Students ("Petya", ((int) (Math.random() * 100)), 1);
        Students StudentE = new Students ("Ivan", ((int) (Math.random() * 100)), 1);

        Students[] a = new Students[5];
        a[0] = StudentA;
        a[1] = StudentB;
        a[2] = StudentC;
        a[3] = StudentD;
        a[4] = StudentE; 

        String NameA = StudentA.getName();

        System.out.println(NameA+" ");
    }
}