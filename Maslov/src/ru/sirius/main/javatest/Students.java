package ru.sirius.main.javatest;
public class Students {

    private String name;

    public int prod; 

    public static int motivation; 

    public Students(String name, int motivation, int prod) {
        this.name = name;
        Students.motivation = motivation;
        this.prod = prod;
    }

    public Students() {
    }

    public int getProd(){
        return prod;
    }

    public int getMot(){
        return Students.motivation;
    }

    public String getName(){
        return name;
    }
 
}
