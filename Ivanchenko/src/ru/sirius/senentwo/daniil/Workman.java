package ru.sirius.seventwo.daniil;
import java.util.Random;

class Workman {
  public double productivity = 1;
  public int desire_to_work;
  public double work_result;

  Workman(){
    this.desire_to_work = 1 + new Random().nextInt(99);
  }
  public void work(){
    this.work_result = this.productivity * this.desire_to_work;
  }
}
