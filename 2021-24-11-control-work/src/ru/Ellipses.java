package ru;

public class Ellipses extends AbstractFigure{

    Ellipses(){
        this.radius = (int) (Math.random() * 5);
    }

    public int getRadius(){
        return radius;
    }
 }