package src.Kontrol;

public class Circle {
    private int radius;

    public Circle(int radius){
        this.radius = radius;
    }

    public double getLength(){
        return 2 * Math.PI * radius;
    }
}
