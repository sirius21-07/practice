package src.Kontrol;

public class Ellipse extends Figure{
    private int firstSemiAxle;
    private int secondSemiAxle;

    public Ellipse(){}

    public Ellipse(int a, int b){
        this.firstSemiAxle = a;
        this.secondSemiAxle = b;
    }

    public double getLength(){
        return Math.PI * (this.firstSemiAxle + this.secondSemiAxle);
    }
}
