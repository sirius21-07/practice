package src.Kontrol;

public class Program {
    public static void main(String[] args) {

        Section[] sectionsArray = new Section[5];
        BrokenLine[] brokenLinesArray = new BrokenLine[5];
        Circle[] circlesArray = new Circle[5];
        Ellipse[] ellipsesArray = new Ellipse[5];
        double sumLengthSec = 0;
        double sumLengthBrok = 0;
        double sumLengthCirc = 0;
        double sumLengthEllipse = 0;
        double LengthOfLines = 0;
        double LengthofFigures = 0;


        for (int i = 0; i < sectionsArray.length; i++) {
            sectionsArray[i] = new Section((int) (Math.random() * 10),(int) (Math.random() * 10),(int) (Math.random() * 10),(int) (Math.random() * 10));
            sumLengthSec += sectionsArray[i].getLength();
        }
        System.out.println("Сумма длин отрезков: " + sumLengthSec);


        for (int i = 0; i < brokenLinesArray.length; i++) {
            int[] a = {(int) (Math.random() * 10), (int) (Math.random() * 10), (int) (Math.random() * 10)};
            int[] b = {(int) (Math.random() * 10), (int) (Math.random() * 10), (int) (Math.random() * 10)};
            brokenLinesArray[i] = new BrokenLine(a,b);
            sumLengthBrok += brokenLinesArray[i].getLength();
        }
        System.out.println("Сумма длин ломаных: " + sumLengthBrok);


        for (int i = 0; i < circlesArray.length; i++) {
            circlesArray[i] = new Circle((int) (Math.random() * 10));
            sumLengthCirc += circlesArray[i].getLength();
        }
        System.out.println("Сумма длин окружностей: " + sumLengthCirc);


        for (int i = 0; i < ellipsesArray.length; i++) {
            ellipsesArray[i] = new Ellipse((int)(Math.random() * 10),(int)(Math.random() * 10));
            sumLengthEllipse += ellipsesArray[i].getLength();
        }
        System.out.println("Сумма длин эллипсов: " + sumLengthEllipse);

        LengthofFigures = sumLengthCirc + sumLengthEllipse;
        LengthOfLines = sumLengthSec + sumLengthBrok;

        System.out.println(" ");

        if(LengthofFigures > LengthOfLines){
            System.out.println("Сумма фигур больше и состовляет: " + LengthofFigures);
        }
        else {
            System.out.println("Сумма линий больше и состовляет: " + LengthOfLines);
        }


    }
}
