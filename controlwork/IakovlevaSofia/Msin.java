package practice.controlwork.IakovlevaSofia;

public class Msin {
    public static void main(String[] args) {
        int sum = 0;
        Hero[] heroes = new Hero[15];
        for (int i = 0; i < 5; i++) {
            heroes[i] = new Luchniki();
            heroes[i+5] = new Kopeichiki();
            heroes[i+10] = new Mag();
        }

        for (int i = 0; i < 15; i++) {
            sum += heroes[i].getHP();
        }
        System.out.println(sum);
    }
}
