package ru.sirius.sevenone.kaymanov.controlwork;

class StorageDevise implements Usb{
    private final int id;
    static int counter=1;
    private String storage;
    {
        id = counter++;
        this.storage = "";
    }
    public void command(char s){
        this.storage += s;
        System.out.printf("По команде устройству-накопителю номер %d на вход пришел символ %s \n", this.id, s);
    }
    public String getStorage(){
        return this.storage;
    }
    public int getId(){
        return this.id;
    }
}
