package ru.sirius.sevenone.kaymanov.controlwork;

public class Main {
    public static int getRandomDiceNumber(int n)
    {
        return (int) (Math.random() * n);
    }

    public static void main(String[] args) {
        final char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        int alphabetLength = alphabet.length;
        StorageDevise[] storageDevises = new StorageDevise[8];
        for (int i = 0; i < 8; i ++){
            storageDevises[i] = new StorageDevise();
        }
        OutputDevise[] outputDevises = new OutputDevise[8];
        for (int i = 0; i < 8; i ++){
            outputDevises[i] = new OutputDevise();
        }
        for (StorageDevise j:storageDevises){
            for (int i = 0; i < 10; i++) {
                j.command(alphabet[getRandomDiceNumber(alphabetLength)]);
            }
        }
        for (OutputDevise j:outputDevises){
            for (int i = 0; i < 10; i++) {
                j.command(alphabet[getRandomDiceNumber(alphabetLength)]);
            }
        }
        for (StorageDevise j:storageDevises){
            System.out.printf("Хранилище устройства-накопителя %d: %s \n",j.getId(),j.getStorage() );
        }
    }
}
