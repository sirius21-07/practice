package ru.sirius.sevenone.kaymanov.controlwork;

class OutputDevise implements Usb{
    private final int id;
    static int counter=1;
    {
        id = counter++;
    }

    public void command(char s) {
        System.out.printf("По команде устройству вывода номер %d на вход пришел символ %s \n", this.id, s);
        System.out.println(s);
    }

}
