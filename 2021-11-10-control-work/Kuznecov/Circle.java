public class Circle extends Figure{
    public double pi;
    public double radius;

    public Circle(double radius) {

        this.radius = radius;
    }
    public double getLength() {

        return (radius * 2) * pi;

    }
}
