
public class Lom extends Segment{
        private int[] x;
        private int[] y;

        public Lom(int[] x, int[] y) {
            this.x = x;
            this.y = y;
        }
        public String getPoint(int i) {
            return "(" + x[i] + "; " + y[i] + ")";
        }
        public double getLength() {
            double len = 0;
            for (int i = 0; i < x.length-1; i++) {
                len = len + Math.sqrt((x[i+1] - x[i]) * (x[i+1] - x[i]) + (y[i+1] - y[i]) * (y[i+1] - y[i]));
            }
            return len;

        }
    }
}
