public class Ellipse extends Figure{

    public int a;
    public int b;
    public double pi;

    public ellipse(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public double getLength() {
        return pi * (a + b);
    }
}
