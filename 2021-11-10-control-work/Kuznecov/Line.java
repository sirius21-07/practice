
public class Line extends Segment {
        private int x1;
        private int y1;
        private int x2;
        private int y2;

        public Line(int x1, int y1, int x2, int y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
        }

        public String getPoint1() {
            return "(" + x1 + "; " + y1 + ")";
        }

        public String getPoint2() {
            return "(" + x2 + "; " + y2 + ")";
        }

        public double getLength() {
            return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        }
    }

}
