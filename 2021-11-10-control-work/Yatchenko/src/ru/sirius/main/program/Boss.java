package ru.sirius.main.program;
import java.util.Random;

class Boss extends AbstractEmployee{
  public static double productivity = 1.5;

  Boss(){
    this.wish_to_work = 1 + new Random().nextInt(99);
  }

  public void work(){
    this.work_result = this.productivity * this.wish_to_work;
  }
}
