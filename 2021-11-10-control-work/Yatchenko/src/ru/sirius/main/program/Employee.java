package ru.sirius.main.program;
import java.util.Random;

class Employee extends AbstractEmployee{
  public static double productivity = 1;
  // public int wish_to_work;
  // public double work_result;

  Employee(){
    this.wish_to_work = 1 + new Random().nextInt(99);
  }

  public void work(){
    this.work_result = this.productivity * this.wish_to_work;
  }
}
