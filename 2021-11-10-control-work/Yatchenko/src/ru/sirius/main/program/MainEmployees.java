package ru.sirius.main.program;
import java.util.Random;


class MainEmployees{
  public static void main(String[] args) {
    int n = 10;
    AbstractEmployee[] arr = new AbstractEmployee[n];
    int switcher = 0;
    for (int i = 0; i < n; i++) {
      switcher = new Random().nextInt(3);
      if (switcher == 2){
        arr[i] = new Employee();
      }
      if (switcher == 1){
        arr[i] = new GoodEmployee();
      } else {
        arr[i] = new Boss();
      }
      arr[i].work();
      if (arr[i].work_result < 80){
        System.out.println(arr[i] + " work is " + arr[i].work_result);
      }
    }
  }
}
