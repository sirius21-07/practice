package ru.sirius.main.program;
import java.util.Random;

abstract class AbstractEmployee{
  public double productivity;
  public int wish_to_work;
  public double work_result;

  abstract public void work();
}
