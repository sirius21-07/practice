package KhabibullinTry2;

public class Point extends D2d {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public String getPoint() {
        return "(" + x + "; " + y + ")";
    }
}
