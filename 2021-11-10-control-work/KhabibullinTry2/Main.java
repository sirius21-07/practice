package KhabibullinTry2;

public class Main {
    public static void main(String[] args) {
        D2d[] d = new D2d[15];
        int s = 0;
        double p = 0;
        do {
            d[s] = new Point((int) (Math.random() * 10), (int) (Math.random() * 10));
            s++;
        } while (s < 5);
        do {
            d[s] = new Line((int) (Math.random() * 10), (int) (Math.random() * 10), (int) (Math.random() * 10), (int) (Math.random() * 10));
            if (d[s].getLength() > p) {
                p = d[s].getLength();
            }
            s++;
        } while (s < 10);
        do {
            int[] a = {(int) (Math.random() * 10), (int) (Math.random() * 10), (int) (Math.random() * 10)};
            int[] b = {(int) (Math.random() * 10), (int) (Math.random() * 10), (int) (Math.random() * 10)};
            d[s] = new CurveLine(a, b);
            if (d[s].getLength() > p) {
                p = d[s].getLength();
            }
            s++;
        } while (s < 15);
        System.out.println(p);

    }
}
