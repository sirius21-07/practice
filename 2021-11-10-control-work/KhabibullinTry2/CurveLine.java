package KhabibullinTry2;

public class CurveLine extends D2d {
    private int[] x;
    private int[] y;

    public CurveLine(int[] x, int[] y) {
        this.x = x;
        this.y = y;
    }
    public String getPoint(int i) {
        return "(" + x[i] + "; " + y[i] + ")";
    }
    public double getLength() {
        double l = 0;
        for (int i = 0; i < x.length-1; i++) {
            l = l + Math.sqrt((x[i+1] - x[i]) * (x[i+1] - x[i]) + (y[i+1] - y[i]) * (y[i+1] - y[i]));
        }
        return l;

    }
}
