package practice.IakovlevaSofia;
public class Main {
    public static void main(String[] args) {
        int num = 0;
        int max = 0;
        int i = 0;
        Figures[] figures = new Figures[20];
        do{
            figures[i] = new Triangle();
            figures[i+5] = new Quadrangle();
            figures[i+10] = new Pentagon();
            figures[i+15] = new Ellipse();
            i++;
        }while(i!=5);
        do{
            if (max < figures[num].getArea())
                max = figures[num].getArea();
            num++;
        }while(num!=20);
        System.out.println(max);

    }
}
