// Описать структуру классов для работы со студентами. Есть обычные студенты и продвинутые, у продвинутых показатель продуктивности равен 2, у обычных - 1. Мотивация у каждого студента - случайное число от 1 до 100. Успеваемость равна произведению показателя продуктивности вида на личную мотивацию, нормализованному в интервале от 0 до 5. Отсортировать их по успеваемости.

package ru.sirius.seven_one.alexander;


public class Main {
    public static void main(String[] args) {

        int lengthArr = 5;
        NormalStudent[] ArrNormalStudent =  new NormalStudent[lengthArr];
        CleverStudent[] ArrCleverStudent = new CleverStudent[lengthArr];
        int i = 0;
        do {
            ArrNormalStudent[i] = new NormalStudent("ivan");
            ArrCleverStudent[i] = new CleverStudent("ivan");
            i++;
        } while (i < lengthArr);


    }
 }