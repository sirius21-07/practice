public class Application {

    public static void main(String[] args) {
        Figure[] figures = new Figure[10];
        figures[0] = new Point(1, 5);
        figures[1] = new Line(new Point(3, 5), new Point(5, 9));
        figures[2] = new Line(new Point(-1, 5), new Point(3, 10));
        figures[3] = new BrokenLine(new Point[] {
                new Point(1, 5),
                new Point(5, 3),
                new Point(8, 2),
                new Point(10, 10)
        });
        figures[4] = new Line(new Point(10, 10), new Point(20, 20));
        figures[5] = new Point(10, 1);
        figures[6] = new BrokenLine(new Point[] {
                new Point(1, 1),
                new Point(2,5),
                new Point(3, 7)
        });
        figures[7] = new Line(new Point(1, 1), new Point(10, 20));
        figures[8] = new Point(1, 5);
        figures[9] = new Line(new Point(7, 10), new Point(11, 25));
        Figure max = figures[0];
        int index = 0;
        for(int i = 1; i < figures.length; i++) {
            Figure current = figures[i];
            if(current.getLength() > max.getLength()) {
                max = current;
                index = i;
            }
        }
        System.out.println("Объект с максимальной длинной: " + max.getName() + ", длина: " + max.getLength() + ", индекс: " + index);
    }

}
