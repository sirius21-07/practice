public interface Figure {

    String getName();
    double getLength();

}
