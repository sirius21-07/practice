public class BrokenLine implements Figure {

    private Line[] lines;

    public BrokenLine(Point[] points) {
        lines = new Line[points.length-1];
        for(int i = 0; i < points.length - 1; i++) {
            lines[i] = (new Line(points[i], points[i+1]));
        }
    }

    @Override
    public String getName() {
        return "Ломаная линия";
    }

    @Override
    public double getLength() {
        double length = 0;
        for(Line line : lines)
            length += line.getLength();
        return length;
    }

}
