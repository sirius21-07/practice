public class Line implements Figure {

    private Point first;
    private Point second;

    public Line(Point first, Point second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public String getName() {
        return "Прямая";
    }

    @Override
    public double getLength() {
        return Math.sqrt(Math.pow(first.getX() - second.getX(), 2) + Math.pow(first.getY() - second.getY(), 2));
    }

}
