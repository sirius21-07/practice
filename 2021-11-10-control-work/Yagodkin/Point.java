public class Point implements Figure {

    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public String getName() {
        return "Точка";
    }

    @Override
    public double getLength() {
        return 0;
    }
}
