package khoshev;

import java.util.Scanner;

public class Main{
	public static void quickSort(Student[] array, int low, int high) {
		if (array.length == 0)
			return;//завершить выполнение если длина массива равна 0
 
		if (low >= high)
			return;//завершить выполнение если уже нечего делить
 
		// выбрать опорный элемент
		int middle = (low + high) / 2;
		int opora = array[middle].GetAcademicPerformance();
 
		// разделить на подмассивы, который больше и меньше опорного элемента
		int i = low, j = high;
		while (i <= j) {
			while (array[i].GetAcademicPerformance() < opora) {
				i++;
			}
 
			while (array[j].GetAcademicPerformance() > opora) {
				j--;
			}
 
			if (i <= j) {//меняем местами
				Student temp = array[i];
				array[i] = array[j];
				array[j] = temp;
				i++;
				j--;
			}
		}
 
		// вызов рекурсии для сортировки левой и правой части
		if (low < j)
			quickSort(array, low, j);
 
		if (high > i)
			quickSort(array, i, high);
	}

	public static void main(String[] args){
		int n = 10;
		Student[] massive = new Student[n];

		int count1 = 0;
		while(count1 < n){
			massive[count1] = new Student();
			++count1;
		}

		massive[0] = new BestStudent();
		int count2 = 0;
		while(count2 < n){
			System.out.println(massive[count2].GetAcademicPerformance());
			++count2;
		}

		System.out.println();

		quickSort(massive, 0, n-1);
		int count3 = 0;
		while(count3 < n){
			System.out.println(massive[count3].GetAcademicPerformance());
			++count3;
		}
		System.out.println();
	}
}