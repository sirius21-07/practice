package khoshev2;

import java.util.*;

public class Main {
    public static void main(String[] args) throws AmountException, SideException, PolygonException{
        Scanner sc = new Scanner(System.in);
        System.out.println("How many polygons do you want to input? ");
        try{
            int n = sc.nextInt();
            if(n < 0)
                throw new AmountException("Error, Amount has to be non-negative number!");
            List<Polygon> figures = new ArrayList<>();
            for(int t = 0; t < n; t++){
                System.out.println("What polygon do you want to input? Write name of the polygon in any register");
                String p = sc.nextLine();
                String q = sc.nextLine().toLowerCase();
                if(q.compareTo("triangle") == 0){
                    System.out.println("Input side of the triangle");
                    Double side = sc.nextDouble();
                    figures.add(new Triangle(side));
                }
                else if(q.compareTo("square") == 0){
                    System.out.println("Input side of the square");
                    Double side = sc.nextDouble();
                    figures.add(new Square(side));
                }
                else if(q.compareTo("hexagon") == 0){
                    System.out.println("Input side of the hexagon");
                    Double side = sc.nextDouble();
                    figures.add(new Hexagon(side));
                }
                else if(q.compareTo("circle") == 0){
                    System.out.println("Input radius of the circle");
                    Double side = sc.nextDouble();
                    figures.add(new Circle(side));
                }
                else
                    throw new PolygonException("Error, Invalid polygon name!");
                if(figures.get(figures.size() - 1).side <= 0)
                    throw new SideException("Error, Side has to be a positive number!");
            }
            double P = 0;
            int i = 0;
            do {
                Polygon k = figures.get(i);
                P += k.Perim();
                if (k.getClass() == Triangle.class)
                    ((Triangle) k).voice();
                else if(k.getClass() == Circle.class)
                    ((Circle) k).voice();
                i += 1;
            }while(i < figures.size());

            System.out.println("The sum of figures' perimeters is " + P);
        } catch(AmountException | SideException e){
            System.out.println(e.getMessage());
        }

    }
}
