package khoshev2;

class Circle extends Polygon{
    // side = radius
    public Circle(Double side){
        this.side = side;
    }
    void voice(){
        System.out.println("I am a circle!");
    }
    @Override
    Double Perim(){
        return 2 * Math.PI * side;
    }
}