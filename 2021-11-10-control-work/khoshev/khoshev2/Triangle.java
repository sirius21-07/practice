package khoshev2;

class Triangle extends Polygon{
    public Triangle(Double side) {
        this.side = side;
    }

    void voice(){
        System.out.println("I am a triangle!");
    }

    @Override
    Double Perim() {
        return 3 * side;
    }
}