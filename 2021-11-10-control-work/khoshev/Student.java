package khoshev;

class Student{
	int kpd = 0;
	int mt = 0;
  
	public Student(){
		this.kpd = 1;
		this.mt = (int)(Math.random()*100) % 100 + 1;
	}

	public int GetAcademicPerformance(){
		if((this.kpd * this.mt) > 100){
			return 5;
	}else{
		return (int)(5*((this.kpd * this.mt * 1.0) / 100.0));
		}
	}
}