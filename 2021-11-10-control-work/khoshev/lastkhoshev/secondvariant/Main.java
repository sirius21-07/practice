package secondvariant;

import java.util.*;
import java.lang.Math;
import java.util.Random;


public class Main {
    public static void main(String[] args){
        Random randNumber = new Random();
        int CntTank = 5 + randNumber.nextInt(6);
        int CntAmphibiousTank = 5 + randNumber.nextInt(6);
        int CntBoat = 5 + randNumber.nextInt(6);

        Technic[] army = new Technic[CntTank + CntAmphibiousTank + CntBoat];
        int i = 0;
        while (i < CntTank){
            army[i] = new Tank();
            i += 1;
        }
        while (i < CntAmphibiousTank){
            army[i] = new AmphibiousTank();
            i += 1;
        }
        while (i < CntBoat){
            army[i] = new Boat();
            i += 1;
        }

        int[] map = new int[20];
        map[0] = 2;
        i = 1;
        while (i < map.length){
            map[i] = randNumber.nextInt(2);
            i += 1;
        }

        int AnsTank = 0;
        int AnsAmphibiousTank = 0;
        int AnsBoat = 0;
        while (i < army.length){
            int j = 0;
            while (j < map.length && army[i].IsMove(map[j])){
                j += 1;
            }
            if(army[i].Type() == 0){
                AnsTank += j;
            }else if(army[i].Type() == 1){
                AnsBoat += j;
            }else if(army[i].Type() == 2){
                AnsAmphibiousTank += j;
            }
            i += 1;
        }
        if(AnsBoat > AnsTank){
            System.out.println("Boat");
        }else{
            System.out.println("Tank");
        }
    }
}