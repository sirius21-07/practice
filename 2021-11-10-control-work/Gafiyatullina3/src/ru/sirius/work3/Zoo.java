package ru.sirius.work3;
import java.util.Random;

class Zoo{
    public static void main (String[] args) {
        Rabbit[] rabbits = new Rabbit[5 + (new Random().nextInt(5))];
        Mouse[] mouses = new Mouse[5 + (new Random().nextInt(5))];
        Snake[] snakes = new Snake[5 + (new Random().nextInt(5))];
        int counter = 0;
        int boxQuan = 0;
        int i = 0;
        while(i < rabbits.length){
            rabbits[i] = new Rabbit();
            counter+=rabbits[i].getVolume();
        }

        i = 0;
        while(i < mouses.length){
            mouses[i] = new Mouse();
            counter += mouses[i].getVolume();
        }
        boxQuan += (int)(counter/6);
        counter = 0;
        i = 0;
        while(i < snakes.length){
            snakes[i] = new Snake();
            counter += snakes[i].getVolume();
        }
        boxQuan += (int)(counter/6);
        System.out.println(boxQuan);
    }
}
