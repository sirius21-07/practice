package src.ru.Sirius3;

// Есть змеи, мыши и кролики.
// Количество каждого вид животного - случайное число от 5 до 10.
// Мышь занимает 1 единицу объема, кролик - 3, змея - 5.
// Емкость коробки - 6 единиц.(по весу)
// Алгоритм заполнения коробок произвольный, но змей нельзя перевозить с другими животными.
// Сколько коробок надо для перевозки змей?
// for

class Main{
  public static void main(String[] args) {
    int tMouse = (int) (Math.random() * 5) + 5;
    int tRabbits = (int) (Math.random() * 5) + 5;
    int tSnakes = (int) (Math.random() * 5) + 5;
    int t = tMouse + tRabbits + tSnakes;
    Animals[] mainmass = new Animals[t];
    for (int i = 0; i < mainmass.length;i++){
      if (i <= tMouse){
        mainmass[i] = new Mouse();
      }
      else if (tMouse <= i && i < tRabbits){
        mainmass[i] = new Rabbits();
      }
      else if (tRabbits <= i && i < tSnakes){
        mainmass[i] = new Snakes();
      }
    }
    int a = 0;
    int kolbox = 0;
    int kolSnakes = 0;
    int kolMouseRabbits = 0;
    for (int i = 0; i < mainmass.length;i++){
      if (mainmass[i] instanceof Mouse || mainmass[i] instanceof Rabbits){
         kolMouseRabbits++;
      }
      else{
        kolSnakes++;
      }
      // if (mainmass[i].volume == 1 || mainmass[i].volume == 3){
      //   kolMouseRabbits++;
      // }
      // if (mainmass[i].volume == 5){
      //   kolSnakes++;
      // }
    }
    a = kolSnakes / 6;
    System.out.println(a);



  }
}
