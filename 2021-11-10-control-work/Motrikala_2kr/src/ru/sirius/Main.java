package src.ru.sirius;

public class Main {
  public static void main(String[] args) {
    Flyable[] flyables = new Flyable[7];

    for (int i = 0; i < 2; i++){
      flyables[i] = new Strateg();
    }
    for (int i = 0; i < 4; i++){
      flyables[i] = new Operativ();
    }
    for (int i = 0; i < 7; i++){
      flyables[i] = new Taktik();
    }

    int target = 11700;
    int counter = 0;

    for (int i = 0; i < 7; i++) {
      if (flyables[i].getDlina() > target) {
        counter += 1;
      }
    }
    System.out.println(counter + " can reach target!");
  }
}
