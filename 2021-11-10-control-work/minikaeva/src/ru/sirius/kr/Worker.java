package ru.sirius.kr;

    public class Worker extends SimpleWorker{
        Worker(){}


        Worker(double productivity, int willing){
            this.productivity = productivity;
            this.willing = willing;
        }

        
        
        public double getProductivity(){
            return this.productivity;
        }

        public int getWilling(){
            return this.willing;
        }
    }