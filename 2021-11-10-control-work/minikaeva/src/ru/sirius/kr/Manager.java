package ru.sirius.kr;

    public class Manager extends SimpleWorker{
        Manager(){}

        Manager(double productivity, int willing){
            this.productivity = productivity;
            this.willing = willing;
        }

       
        
        public double getProductivity(){
            return this.productivity;
        }

        public int getWilling(){
            return this.willing;
        }
    }