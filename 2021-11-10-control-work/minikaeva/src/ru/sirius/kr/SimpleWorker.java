package ru.sirius.kr;

    public class SimpleWorker{
        public double productivity;
        public int willing;

        SimpleWorker(){}

        
        SimpleWorker(double productivity, int willing){
            this.productivity = productivity;
            this.willing = willing;
        }

        
        public double getProductivity(){
            return this.productivity;
        }

        public int getWilling(){
            return this.willing;
        }
        
    }