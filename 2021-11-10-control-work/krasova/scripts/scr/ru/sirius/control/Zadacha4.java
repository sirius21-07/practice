package ru.sirius.control;

class Zadacha4{
  public static void main(String[] args){

    double sumWorkers = 0;
    Workers[] array = new Workers[15];
    for (int i = 0; i < array.length; i++){
      if (i % 3 == 0){
        array[i] = new Employee();
      }
      if (i % 3 == 1){
        array[i] = new GoodEmployee();
      }
      if (i % 3 == 2){
        array[i] = new Manager();
      }
    }

    for (int i = 0; i < array.length; i++) {
      if (array[i] instanceof Employee){
        array[i].setResult(((Employee)array[i]).productivity);
        sumWorkers += array[i].getResult();
      }
      if (array[i] instanceof GoodEmployee){
        array[i].setResult(((GoodEmployee)array[i]).productivity);
        sumWorkers += array[i].getResult();
      }
      if (array[i] instanceof Manager){
        array[i].setResult(((Manager)array[i]).productivity);
        sumWorkers += array[i].getResult();
      }
    }
    System.out.println((double)sumWorkers/array.length);
  }
}
