package hlapov;

class GoodWorker extends Personal{
  static double productivity = 2;

  GoodWorker(){
    this.desireToWork = (double)((int) (Math.random()*100 + 1));

  }

  public double getWork(){
    return productivity* this.desireToWork;
  }
}
