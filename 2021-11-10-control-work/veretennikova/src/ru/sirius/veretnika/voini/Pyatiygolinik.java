
package ru.sirius.veretnika.voini;

class Pyatiygolinik extends Figure {

    Pyatiygolinik(int[] sides) throws NotaFigureException {
        super(sides);
        if (sides.length != 5) {
            throw new NotaFigureException();
        }
    }

    public void countSquare() {
        this.square = (sides[0] * sides[1] * sides[2]) * 0.5;
    }

    public double getSquare() {
        return this.square;
    }
}
