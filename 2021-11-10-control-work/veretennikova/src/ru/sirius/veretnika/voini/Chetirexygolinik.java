package ru.sirius.veretnika.voini;

class Chetirexygolinik extends Figure {
    Chetirexygolinik(int[] sides) throws NotaFigureException {
        super(sides);
        if (sides.length != 4) {
            throw new NotaFigureException();
        }
    }

    public void countSquare() {
        this.square = sides[0] * sides[1];
    }

    public double getSquare() {
        return this.square;
    }
}
