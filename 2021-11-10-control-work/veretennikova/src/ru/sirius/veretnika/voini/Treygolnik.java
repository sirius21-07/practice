package ru.sirius.veretnika.voini;

class Treygolnik extends Figure {

    Treygolnik(int[] sides) throws NotaFigureException {
        super(sides);
        if (sides.length != 3) {
            throw new NotaFigureException();
        }
    }

    public String toString() {
        return "привет я треугольник";
    }

    public void countSquare() {
        int halfPerimter = (sides[0] + sides[1] + sides[2]) / 2;
        this.square = Math
                .sqrt(halfPerimter * (halfPerimter - sides[0]) * (halfPerimter - sides[1]) * (halfPerimter - sides[2]));
    }

    public double getSquare() {
        return this.square;
    }

}