package ru.sirius.veretnika.voini;

abstract class Figure {
    public int[] sides;
    protected double square;

    protected Figure(int[] sides) {
        this.sides = sides;
    }

    abstract void countSquare();

    abstract double getSquare();

}

// getSquare