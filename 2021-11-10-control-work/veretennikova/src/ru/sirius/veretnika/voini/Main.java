package ru.sirius.veretnika.voini;

//Описать структуру классов для работы с геометрическими фигурами - правильными многоугольниками (треугольник, четырехугольник, пятиугольник), эллипсами. 
//Треугольник и эллипс могут "вывести себя" на экран особенным образом. Найти фигуру с максимальной площадью.
public class Main {
    double sides;

    public static void main(String[] args) {
        Figure[] arr = new Figure[5];
        try {
            arr[0] = new Treygolnik(new int[] { 2, 2, 2 });
            arr[1] = new Chetirexygolinik(new int[] { 3, 3 });
            arr[2] = new Pyatiygolinik(new int[] { 5, 5, 5, 5, 5 });
            arr[3] = new Elipsis(new int[] { 5, 5 });
            // arr[4] = new Treygolnik(new int[] {2, 2, 2, 2});
        } catch (Exception e) {
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] instanceof Treygolnik) {
                System.out.println(arr[i].toString());
                arr[i].countSquare();
                System.out.println(arr[i].getSquare());
            }
            if (arr[i] instanceof Chetirexygolinik) {
                System.out.println("2");
                arr[i].countSquare();
                System.out.println(arr[i].getSquare());
            }

            if (arr[i] instanceof Pyatiygolinik) {
                System.out.println("3");
                arr[i].countSquare();
                System.out.println(arr[i].getSquare());

            }
            if (arr[i] instanceof Elipsis) {
                System.out.println("4");
                System.out.println(arr[i].toString());
                arr[i].countSquare();
                System.out.println(arr[i].getSquare());
            }
        }
    }

}
