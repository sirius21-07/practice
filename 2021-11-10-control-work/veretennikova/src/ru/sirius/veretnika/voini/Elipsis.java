package ru.sirius.veretnika.voini;

class Elipsis extends Figure {
    Elipsis(int[] sides) throws NotaFigureException {
        super(sides);
        if (sides.length != 2) {
            throw new NotaFigureException();
        }
    }

    public String toString() {
        return "привет я элипсис";
    }

    public void countSquare() {
        this.square = Math.PI * sides[0] * sides[1];
    }

    public double getSquare() {
        return this.square;
    }

}