package ru.sirius.group.Denis;

public class Main {
  public static void main(String[] args) {
    // Triangle triangle = new Triangle();
    // System.out.println(triangle.Perimetr());

    Figura[] figura = new Figura[15];
    double s = 1;
    int i = 0;
    while (i < 3) {
      figura[i] = new Circle();
      i++;
    }
    while (i < 6) {
      figura[i] = new Triangle();
      i++;
    }
    while (i < 9) {
      figura[i] = new Quadrilateral();
      i++;
    }
    while (i < 12) {
      figura[i] = new Hexagon();
      i++;
    }
    i = 0;
    while (i < 15) {
      s += figura[i].Perimetr();
      i++;
    }

    System.out.println(s);

    // figura[0] = new Triangle();
    // Figura triangle = figura[0];
    // System.out.println(triangle.Perimetr() + " " +triangle.VivodT());
  }
}
