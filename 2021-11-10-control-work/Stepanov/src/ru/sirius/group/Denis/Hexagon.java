package ru.sirius.group.Denis;

public class Hexagon extends Figura {
  public double a = 6;
  public double b = 9;
  public double c = 3;
  public double x = 9;
  public double g = 21;
  public double f = 1;
  public double sum = 0;

  @Override
  public double Perimetr() {
    sum = a + b + c + x + g + f;
    return sum;
  }
}
