package ru.sirius.artur.main;

public class Employee {
    public final int wish;
    public double productivityIndicator;

    public double work() {
        return wish * productivityIndicator;
    }

    Employee(double productivityIndicator) {
        this.productivityIndicator = productivityIndicator;
        this.wish = (int) (Math.random() * 100 + 1);
    }
}
