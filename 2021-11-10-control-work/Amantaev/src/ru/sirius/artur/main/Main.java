package ru.sirius.artur.main;

public class Main {
    public static void main(String[] args) {
        Employee[] employees = new Employee[10];
        int amountOfWorkers = 3;
        int amountOfGoodWorkers = 3;
        for (int i = 0; i < amountOfWorkers; i++) {
            employees[i] = new Worker();
        }
        for (int i = amountOfWorkers; i < amountOfWorkers + amountOfGoodWorkers; i++) {
            employees[i] = new GoodWorker();
        }
        for (int i = amountOfWorkers + amountOfGoodWorkers; i < employees.length; i++) {
            employees[i] = new Manager();
        }

        for (int i = 0; i < employees.length; i++) {
            double resultOfWork = employees[i].work();
            if (resultOfWork < 80) {
                if (employees[i] instanceof Worker) {
                    System.out.println("Worker " + i + ": " + resultOfWork);
                }
                if (employees[i] instanceof GoodWorker) {
                    System.out.println("GoodWorker " + (i - amountOfWorkers) + ": " + resultOfWork);
                }
                if (employees[i] instanceof Manager) {
                    System.out.println("Manager " + (i - amountOfWorkers - amountOfGoodWorkers) + ": " + resultOfWork);
                }
            }
        }
    }
}
