package geometry;

class Main {
  public static void main(String[] args) {
    int n = 4;
    Figures[] mass = new Figures[n];
    mass[0] = new Triangle("Треугольник");
    mass[1] = new Quadrangle("Четырёхугольник");
    mass[2] = new Pentagon("Пятиугольник");
    mass[3] = new Ellipse("Элипс");
    int square = 0;
    ((Triangle)(mass[0])).answer();
    ((Ellipse)(mass[3])).answer();
    int i = 0;
    int x = mass[0].a;
    do {
      if (mass[i].a < mass[i + 1].a) {
        x = mass[i + 1].a;
      }
      i++;
    } while (i < n);
    System.out.println(x);
  }
}
