package ru.sirius.rudolf.cw;

public class Cw {
    public static void main(String[] args) {
        Hero[] heroes = new Hero[15];
        int s = 0;
        int i = 0;
        while (i < 5) {
            heroes[i] = new Mage();
            i++;
        }
        while (i < 10) {
            heroes[i] = new Archer();
            i++;
        }
        while (i < 15) {
            heroes[i] = new Polearm();
            i++;
        }
        i = 0;
        while (i < 15) {
            if (heroes[i].isRange()) {
                s += heroes[i].getDmg();
            }
            i++;
        }
        System.out.println(s);
    }
}

