package ru.sirius.rudolf.cw;

public class Hero implements Ranged{

    private int maxHP = (int) (Math.random() * 100);
    private int dmg = (int) (Math.random() * 100);
    boolean range;

    public int getMaxHP() {
        return maxHP;
    }

    public int getDmg() {
        return dmg;
    }

    public boolean isRange() {
        return false;
    }
}
