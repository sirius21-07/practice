package ru.sirius.rudolf.cw;

public interface Ranged {
    public boolean isRange();
}
