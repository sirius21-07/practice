package src.ru.sirius.Ilya;

public class Main {
    public static void main(String[] args) {
        Figure[] figure = {new Rectangle(8, 12), 
                           new Triangle(5, 8, 5), 
                           new Square (5, 5),
                           new Pentagon (5, 2),
                           new Ellipse (2, 5)};
        
        for(Figure fig : figure)
            System.out.println(fig.getName() + ": площадь = " + fig.getArea());
    }
}