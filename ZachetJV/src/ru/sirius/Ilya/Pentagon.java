package src.ru.sirius.Ilya;

public class Pentagon extends Figure {
    private static final String NAME = "Пятиугольник";
    
    private double width;
    private double radius;
    
    public Pentagon(double width, double radius) {
        this.width = width;
        this.radius = radius;
    }
    
    @Override
    public double getArea() {
        return radius * ((width * 4)/2);
    }
    
    @Override
    public String getName() {
        return NAME;
    }
    
    public double getWidth() {
        return width;
    }
    
    public void setWidth(double width) {
        this.width = width;
    }
    
    public double getRadius() {
        return radius;
    }
    
    public void setRadius(double radius) {
        this.radius = radius;
    }
}
