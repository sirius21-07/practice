package src.ru.sirius.Ilya;

public class Ellipse extends Figure {
    private static final String NAME = "Эллипс";
    
    private double width;
    private double height;
    
    public Ellipse(double width, double height) {
        this.width = width;
        this.height = height;
    }
    
    @Override
    public double getArea() {
        return width * height * 3.14;
    }
    
    @Override
    public String getName() {
        return NAME;
    }
    
    public double getWidth() {
        return width;
    }
    
    public void setWidth(double width) {
        this.width = width;
    }
    
    public double getHeight() {
        return height;
    }
    
    public void setHeight(double height) {
        this.height = height;
    }
}
