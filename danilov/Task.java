package danilov;

public class Task {
    public static void main(String[] args){
        Man[] worker = new Man [5];
        worker[0] = new Worker();
        worker[1] = new Worker();
        worker[2] = new GoodWorker();
        worker[3] = new GoodWorker();
        worker[4] = new Boss();
        int i = 0;
        int count = 0;

        do {
            System.out.println("Результат " +(i+1)+ " работника: " +(int)worker[i].res);
            //Не могу понять, как мне вызвать res работников, а не людей в целом, если массив создан людей
            //Пытался проверкой через принадлежность обьекта, все равно программа берет значения от Man
            if (worker[i].res < 80){
                count++;
            }
            i++;
        } while (i < worker.length);

        System.out.println("Кол-во работников, результат которых меньше 80: " +count);
    }
}

