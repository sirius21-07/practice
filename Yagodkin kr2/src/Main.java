import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {

    private static List<Student> studentList = new ArrayList<>();

    public static void main(String[] args) {
        createStudents();
        System.out.println("----------------- ДВОЕЧНИКИ (ОЦЕНКА НИЖЕ 2.6) -----------------");
        System.out.println(studentList.stream().filter(student -> student.getMark() < 2.6).collect(Collectors.toList()));
    }

    private static void createStudents() {
        Random random = new Random();
        for (int i = 1; i <= 100; i++) {
            int productivity = (Math.random() <= 0.5) ? 1 : 2;
            int motivation = random.nextInt(100) + 1;
            double mark = (productivity * motivation) / 40.0; //Интервал от 0.0 до 5.0
            studentList.add(new Student(
                    i,
                    "Student number " + i,
                    productivity == 2 ? ProductivityType.ADVANCED : ProductivityType.COMMON,
                    motivation,
                    mark
            ));
        }
    }
}