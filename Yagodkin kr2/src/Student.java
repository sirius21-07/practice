public class Student {
    private int id;
    private String name;
    private ProductivityType productivityType;
    private int motivationLevel;
    private double mark;

    public Student(int id, String name, ProductivityType productivityType, int motivationLevel, double mark) {
        this.id = id;
        this.name = name;
        this.productivityType = productivityType;
        this.motivationLevel = motivationLevel;
        this.mark = mark;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ProductivityType getProductivityType() {
        return productivityType;
    }

    public int getMotivationLevel() {
        return motivationLevel;
    }

    public double getMark() {
        return mark;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProductivityType(ProductivityType productivityType) {
        this.productivityType = productivityType;
    }

    public void setMotivationLevel(int motivationLevel) {
        this.motivationLevel = motivationLevel;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", productivityType=" + productivityType.getValue() +
                ", motivationLevel=" + motivationLevel +
                ", mark=" + mark +
                "}\n";
    }
}