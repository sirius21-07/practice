public enum ProductivityType {
    COMMON(1),
    ADVANCED(2);

    private final int value;

    ProductivityType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}