public class Chair extends Furniture {
    public  final  int WEIGHT = 1;

    @Override
    public int getWeight() {
        return WEIGHT;
    }

    @Override
    public String toString() {
        return "Стул";
    }
}
