import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Car car = new Car(10);

        Furniture[] furniture = init();

        int pointer = 0;

        int counter = 0;

        DeliveryPoint deliveryPoint = new DeliveryPoint();
        do {

            if (!car.addEntity(furniture[pointer])) {
                deliveryPoint.arrival(car);
                counter++;
            } else {
                pointer++;
            }
        } while (pointer < furniture.length);

        deliveryPoint.arrival(car);
        counter++;

        System.out.print("Склад после перевозок: ");
        deliveryPoint.print();

        System.out.println("Необходимо " + counter + " машин для перевозки");
    }

    public static Furniture[] init() {
        Random random = new Random();
        int chairCount = random.nextInt( 6) + 5;
        int tableCount = random.nextInt( 6) + 5;
        int dresserCount = random.nextInt( 6) + 5;
        Furniture[] furniture = new Furniture[chairCount + tableCount + dresserCount];
        int pointer = 0;
        for (int i = 0; i < chairCount; i++) {
            furniture[pointer] = new Chair();
            pointer++;
        }

        for (int i = 0; i < tableCount; i++) {
            furniture[pointer] = new Table();
            pointer++;
        }

        for (int i = 0; i < dresserCount; i++) {
            furniture[pointer] = new Dresser();
            pointer++;
        }

        System.out.println("Стульев: " + chairCount + ", столов: " + tableCount + ", коммодов: " + dresserCount);

        return furniture;
    }
}
