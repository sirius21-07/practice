import java.util.List;

public class DeliveryPoint {
    private Entity[] storage;
    private int capacity = 100;
    int pointer = 0;
    DeliveryPoint() {
        storage = new Entity[capacity];
    }

    void print() {
        for (int i = 0; i < pointer; i++) {
            System.out.print(storage[i] + " ");
        }
        System.out.println();
    }

    public void arrival(ShippingTransport transport) {
        for (Entity entity : transport.getArrival()) {
            if (pointer >= capacity) {
                return;
            }
            if (entity != null) {
                storage[pointer] = entity;
                pointer++;
            }
        }
    }
}
