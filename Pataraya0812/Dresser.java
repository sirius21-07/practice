public class Dresser extends Furniture {
    private final int WEIGHT = 7;

    @Override
    public int getWeight() {
        return WEIGHT;
    }

    @Override
    public String toString() {
        return "Комод";
    }
}
