public class Car implements ShippingTransport{
    Entity[] entities;

    private int capacity;

    private final int defaultCapacity = 10;

    private int pointer = 0;

    Car() {
        entities = new Entity[defaultCapacity];
    }

    Car(int capacity) {
        this();
        this.capacity = capacity;
    }

    boolean addEntity(Entity entity) {
        if (getFreeWeight() < entity.getWeight()) {
            return false;
        } else {
            if (pointer >= capacity) {
                return false;
            }
            entities[pointer] = entity;
            pointer++;
            return true;
        }
    }

    int getFreeWeight() {
        int counter = 0;
        for (int i = 0; i < pointer; i++) {
            counter += entities[i].getWeight();
        }
        return capacity - counter;
    }

    @Override
    public Entity[] getArrival() {
        Entity[] e = entities;
        entities = new Entity[capacity];
        pointer = 0;
        return e;
    }
}
