public class Table extends Furniture {
    private final int WEIGHT = 4;

    @Override
    public int getWeight() {
        return WEIGHT;
    }

    @Override
    public String toString() {
        return "Стол";
    }
}
