public abstract class Furniture extends Entity {
    private final int WEIGHT = 1;

    @Override
    public int getWeight() {
        return WEIGHT;
    }

    @Override
    public String toString() {
        return "Мебель";
    }
}
